REM Generates a Self signed Cert, that can be used a 'CA' or 'Root' Cert to sign others

openssl req -x509 -newkey rsa:2048 -keyout %1.key -out %1.cer -days 1000 -config .\openssl.cnf -sha256 -subj "/CN=%1"
