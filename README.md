# Just a few script to help with using OpenSSL  

`GenRootCert.bat <Cert CN>` -Generates a Self signed Cert, that can be used a 'CA' or 'Root' Cert to sign others  
`GenCert.bat <Cert CN>`     -Generate a Cert signed by a 'CA' or 'Root'  
`ViewRequest.bat <Cert CN>` -View a text version of a request file CSR  

*Remember the cert's key files are "the keys to the kingdom", store them offline or in KeePass*  

**What the switches all do:**   
*CAcreateserial* - Generate serial numbers automatically   
*reg*            - Make a cert request  
*x509*           - Cert Type    
*new*            - New Request  
*newkey*         - Generate a new random key  
*keyout*         - File to store key (can be pem or cer)  
*out*            - File to write cert to  
*days*           - Duration of Cert  
*node*           - Don't prompt for passwords  
*config*         - Location of openssl.cnf file  
*subj*           - Subnetname of Cert must start with "/CN="  
*text*           - Show the text form of a request  
*noout*          - Do not output a REQ file  
*sha256*         - Use sha256 algorithm  
*CA*             - The root certs cer file  
*CAKey*          - The root certs private key  
*CAcreateserial* - Generate serial numbers automatically  
*pkcs12*         - Use a PKCS12 container type  
*inkey*          - Cert's private key file  
*export*         - output PKCS12 file