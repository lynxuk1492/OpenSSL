REM View the content of a Cert Request file CSR
If "%1" == "" Goto _Usage

openssl req -in %1.csr -noout -text

Goto _End
:_Usage
ViewRequest <CN of Request>

:_End
