REM Generate a Cert signed by a 'CA' or 'Root'

REM First create a cert request
openssl req -new -newkey rsa:2048 -keyout %1.key -out %1.csr -sha256 -nodes -config openssl.cnf -subj "/CN=%1"

REM Create Cert and sign it with the root cert
openssl x509 -req -days 730 -in %1.csr -CA %2.cer -CAkey %2.key -out %1.cer -CAcreateserial -sha256

REM Build a pkcs12 Container with the private key
openssl pkcs12 -export -out %1.pfx -inkey %1.key -in %1.cer
